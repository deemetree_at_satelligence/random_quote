# Random Quote
A simple python docker container printing a random quote to `stdout`.
```bash 
$ python3 print_quote.py 
Quote of the time! 2019-10-04 11:49:53.101798: "God damn that's a pretty fucking good milkshake." -- Vincent Vega
```

## Docker image
Docker image available at a public docker hub: https://hub.docker.com/r/deemetreeats11/random-quote

### Usage
```bash
$ docker run --rm deemetreeats11/random-quote
```

## Gitlab repository
Image source can be found here: https://gitlab.com/deemetree_at_satelligence/random_quote

## Funzies
- Uses mypy
- Uses f-strings
